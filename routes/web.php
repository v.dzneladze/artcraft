<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::group(['prefix' => LaravelLocalization::setLocale()], function () {
	/** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/

	Route::get('/', 'HomeController@index')->name('home');
	Route::get('/shop', 'ProductController@index')->name('shop');
	Route::get('/sale', 'HomeController@index')->name('sale');
	Route::get('/news', 'HomeController@index')->name('news');
	Route::get('/blog', 'BlogController@index')->name('blog');
	Route::get('/about', 'AboutController@index')->name('about');
	Route::get('/contact', 'ContactController@index')->name('contact');
});

/** OTHER PAGES THAT SHOULD NOT BE LOCALIZED **/

Auth::routes();

// Route::get('/', function () {
// 	return view('index');
// });
// Route::get('/home', 'HomeController@index')->name('home');
