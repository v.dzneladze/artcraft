<?php

namespace ArtCraft\Http\Controllers;

use Illuminate\Support\Facades\App;

class HomeController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		// $this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view('site.home');
	}

	public function locale() {
		return App::getLocale();
	}
}
