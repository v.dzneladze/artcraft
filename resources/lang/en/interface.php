<?php

return [

	'menu' => [
		'Home' => 'Home',
		'AboutUs' => 'About Us',
		'Contact' => 'Contact',
		'Shop' => 'Shop',
		'News' => 'News',
		'Sale' => 'Sale',
		'Blog' => 'Blog',
	],

	'lang' => [
		'en' => 'ENG',
		'ka' => 'GEO',
		'ru' => 'RUS',
	],

	'announcement' => [
		'Free_shipping_for_standard_order_over_$100' => 'Free shipping for standard order over $100',
		'High_quality_handmade_items' => 'High quality handmade items ',
		'join_us' => 'Join Us',

	],

];
