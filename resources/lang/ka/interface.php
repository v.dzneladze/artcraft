<?php

return [

	'menu' => [
		'Home' => 'მთავარი',
		'AboutUs' => 'ჩვენს შესახებ',
		'Contact' => 'კონტაქტი',
		'Shop' => 'მაღაზია',
		'News' => 'სიახლეები',
		'Sale' => 'აქცია',
		'Blog' => 'ბლოგი',
	],
	'lang' => [
		'en' => 'ENG',
		'ka' => 'GEO',
		'ru' => 'RUS',
	],

	'announcement' => [
		'Free_shipping_for_standard_order_over_$100' => 'უფასო მიტანის სერვისი სტანდარტულ შეკვეთაზე 100$ - ის ზემოთ ',
		'High_quality_handmade_items' => 'მაღალი ხარისხის ხელნაკეთი ნივთები',
		'join_us' => 'შემოგვიერდით',
	],
];
